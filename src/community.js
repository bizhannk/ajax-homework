const wrapper = document.querySelector('#wrapper');

export async function renderCommunity(url) {
  const res = await fetch(url, {
    method: 'GET',
    mode: 'cors',
    cache: 'no-cache',
    credentials: 'same-origin',
    headers: {
      'Content-Type': 'application/json',
    },
    redirect: 'follow',
    referrerPolicy: 'no-referrer',
  });
  const data = await res.json();

  data.map((item) => {
    return (wrapper.innerHTML += `
      <div class="card">
      <img class="avatar" src="${item.avatar}" alt="avatar" />
      <p>
      Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
      eiusmod tempor incididunt ut labore et dolor.
      </p>
      <small class="uppercase">${item.firstName} ${item.lastName}</small>
      <p>${item.position}</p>
      </div>
      `);
  });
}
